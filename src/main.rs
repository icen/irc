use arrayvec::ArrayVec;
use chrono;
use mio;
use std::fmt::Display;
use std::io::{Read, Write};
use std::iter::Iterator;
use std::{fmt, io, mem, net, ptr, str};

static mut CLIENTS: u64 = 0;
static mut CHANNELS: u64 = 0;
static mut READ: u64 = 0;
static mut WRITE: u64 = 0;
static mut CLOSE: u64 = 0;
static mut DELAYED: u64 = 0;
static mut RESPOND: u64 = 0;
static mut USE_REPLY: u64 = 0;
const NO_CONN: mem::MaybeUninit<mio::net::TcpStream> = mem::MaybeUninit::uninit();
const NO_ADDR: mem::MaybeUninit<net::IpAddr> = mem::MaybeUninit::uninit();
static mut CONNS: [mem::MaybeUninit<mio::net::TcpStream>; 64] = [NO_CONN; 64];
static mut NICKS: [S<8>; 64] = [S::new(); 64];
static mut USERNAMES: [S<32>; 64] = [S::new(); 64];
static mut REALNAMES: [S<32>; 64] = [S::new(); 64];
static mut ADDRS: [mem::MaybeUninit<net::IpAddr>; 64] = [NO_ADDR; 64];
static mut STATES: [State; 64] = [State::Disconnected; 64];
static mut CHANNEL_NAMES: [S<32>; 64] = [S::new(); 64];
static mut MEMBERS: [u64; 64] = [0; 64];
static mut INPUT: ArrayVec<u8, 512> = ArrayVec::new_const();
static mut OUTPUT: ArrayVec<u8, 512> = ArrayVec::new_const();
static mut REPLY: ArrayVec<u8, 512> = ArrayVec::new_const();
const CONN: mio::Token = mio::Token(64);
static mut CREATION_TIME: mem::MaybeUninit<chrono::DateTime<chrono::Utc>> = mem::MaybeUninit::uninit();

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum State {
    Disconnected,
    Handshake { stage: u8 },
    Welcomed { stage: u8 },
    Ready,
    Delayed(Delay),
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum Delay {
    Retry,
    Names { channels: u64, sent_members: u64, was_joining: bool },
    Joining { channels: u64 },
    Parting { channels: u64 },
    Listing { channels: u64 },
    WhoIs { users: u64, channel: u8 },
}

macro_rules! write_reply { ($slot:ident, $fmt:literal, $($params:expr),*) => (prepare_buffer!(REPLY, $slot, $fmt, $($params),*)) }
macro_rules! write_outbound { ($slot:ident, $fmt:literal, $($params:expr),*) => (prepare_buffer!(OUTPUT, $slot, $fmt, $($params),*)) }
macro_rules! prepare_buffer {
    ($buf:ident, $slot:ident, $fmt:literal, $($params:expr),*) => ( {
        unsafe { $buf.clear(); }
        if let Err(e) = write!(unsafe { &mut $buf }, $fmt, $($params),*) {
            match message_too_long($slot) {
                Ok(()) => {
                    eprintln!("[{}] {} Expected error from over-long message, found Ok!", $slot, now());
                    return abandon($slot, e);
                },
                Err(_) => ()
            };
            return Err(e);
        }
    })
}

fn main() -> io::Result<()> {
    unsafe { CREATION_TIME = mem::MaybeUninit::new(now()) }
    let mut poll = mio::Poll::new()?;
    let mut events = mio::Events::with_capacity(128);
    let mut listener = mio::net::TcpListener::bind(net::SocketAddr::V4(net::SocketAddrV4::new(net::Ipv4Addr::UNSPECIFIED, 36667)))?;
    poll.registry().register(&mut listener, CONN, mio::Interest::READABLE)?;
    loop {
        if let Err(e) = run(&mut poll, &mut events, &mut listener) {
            eprintln!("{} Error: {}", now(), e);
        }
    }
}

fn run(poll: &mut mio::Poll, events: &mut mio::Events, listener: &mut mio::net::TcpListener) -> io::Result<()> {
    if delayed(0) == 0 {
        poll.poll(events, None)?;
        for event in events.iter() {
            match event.token() {
                CONN => accept(listener, poll)?,
                mio::Token(slot) => {
                    if event.is_readable() {
                        readable(1 << slot);
                    }
                    if event.is_writable() {
                        writable(1 << slot);
                    }
                }
            }
        }
    }

    for slot in bits(delayed(0)) {
        if let Err(e) = resume(slot) {
            eprintln!("[{}] {} Resuming: {}", slot, now(), e);
        }
        flush();
    }

    if delayed(0) != 0 {
        return Ok(());
    }

    for slot in bits(readable(0)) {
        while should_read(slot) {
            if let Err(e) = respond(slot) {
                eprintln!("[{}] {} Input: {}", slot, now(), e);
            }
            flush();
        }
    }
    Ok(())
}

fn accept(listener: &mut mio::net::TcpListener, poll: &mut mio::Poll) -> io::Result<()> {
    match listener.accept() {
        Ok((mut conn, addr)) => unsafe {
            eprintln!("Connection established from {}", addr);
            let slot = CLIENTS.trailing_ones() as usize;
            if slot > 63 {
                eprintln!("Rejecting connection: all slots full");
                return conn.shutdown(net::Shutdown::Both);
            }

            poll.registry().register(&mut conn, mio::Token(slot), mio::Interest::READABLE | mio::Interest::WRITABLE)?;
            CONNS[slot].write(conn);
            ADDRS[slot].write(addr.ip());
            STATES[slot] = State::Handshake { stage: 0 };
            CLIENTS |= 1 << slot;
            Ok(())
        },
        Err(e) if e.kind() == io::ErrorKind::WouldBlock => Ok(()),
        Err(e) => Err(e),
    }
}

fn disconnect(slot: usize) -> io::Result<()> {
    if !client(slot) {
        return Ok(());
    }

    if state(slot) != State::Disconnected {
        write_outbound!(slot, ":{} QUIT :Reason: Client has hung up\r\n", nick(slot));
        outbound(connected(0) & !(1 << slot));
    }

    unsafe {
        CLIENTS &= !(1 << slot);
        READ &= !(1 << slot);
        WRITE &= !(1 << slot);
        CLOSE &= !(1 << slot);
        RESPOND &= !(1 << slot);
        let mut m_conn = NO_CONN;
        let mut m_addr = NO_ADDR;

        ptr::swap(CONNS[slot].as_mut_ptr(), m_conn.as_mut_ptr());
        ptr::swap(ADDRS[slot].as_mut_ptr(), m_addr.as_mut_ptr());
        let conn = m_conn.assume_init();
        let addr = m_addr.assume_init();

        STATES[slot] = State::Disconnected;
        NICKS[slot] = S::new();
        USERNAMES[slot] = S::new();
        REALNAMES[slot] = S::new();

        for s in bits(CHANNELS) {
            MEMBERS[s] &= !(1 << slot);
            if MEMBERS[s] == 0 {
                destroy_channel(s);
            }
        }

        eprintln!("[{}] Closing connection to {}", slot, addr);
        conn.shutdown(net::Shutdown::Both)
    }
}

fn resume(slot: usize) -> io::Result<()> {
    match state(slot) {
        State::Delayed(Delay::Retry) | State::Ready | State::Disconnected => unsafe {
            DELAYED &= !(1 << slot);
            Ok(())
        },
        State::Delayed(Delay::Parting { channels }) => part(slot, channels),
        State::Delayed(Delay::Names { channels, sent_members, was_joining }) => names(slot, channels, sent_members, was_joining),
        State::Delayed(Delay::Joining { channels }) => join(slot, channels),
        State::Delayed(Delay::Listing { channels }) => list(slot, channels),
        State::Delayed(Delay::WhoIs { users, channel }) => who(slot, users, channel),
        State::Welcomed { .. } => send_welcome(slot),
        State::Handshake { .. } => Ok(()),
    }
}

fn respond(slot: usize) -> io::Result<()> {
    let (msg, len) = message(slot)?;
    println!("[{}] {} < {:?}", slot, now(), msg);

    let (_origin, params, tail) = split_msg(msg.trim());
    let mut words = params.split(' ');
    let result = if let Some(command) = words.next() {
        match command {
            "CAP" => cap_command(slot, words.next(), tail),
            "PASS" => Ok(()),
            "NICK" => nick_command(slot, words.next()),
            "USER" => user_command(slot, words.next(), words.next(), words.next(), words.next().or(tail)),
            "JOIN" => join_command(slot, words.next()),
            "PRIVMSG" => privmsg_command(slot, words.next(), tail.or(words.next())),
            "PART" => part_command(slot, words.next()),
            "QUIT" => quit_command(slot, tail),
            "DUMP" => Ok(dump()),
            "PING" => ping_command(slot, words.next().or(tail)),
            "MODE" => mode_command(slot, words.next()),
            "LIST" => list_command(slot, words.next()),
            "WHO" => who_command(slot, words.next()),
            c => unknown_command(slot, c),
        }
    } else {
        Ok(())
    };

    unsafe {
        INPUT.copy_within(len..INPUT.len(), 0);
        INPUT.set_len(INPUT.len() - len);
    }

    result
}

fn cap_command(slot: usize, sub: Option<&str>, tail: Option<&str>) -> io::Result<()> {
    let n = match state(slot) {
        State::Handshake { .. } => "*",
        _ => nick(slot),
    };

    let cmd = match sub {
        Some(cmd) => cmd,
        None => return needs_more_params(slot, "CAP"),
    };

    if ["LS", "LIST"].contains(&cmd) {
        write_reply!(slot, ":server CAP {} {} :\r\n", n, cmd);
        reply(1 << slot);
        if let State::Handshake { stage } = state(slot) {
            set_state(slot, State::Handshake { stage: stage | (1 << 7) });
        }
    } else if cmd == "END" {
        if let State::Handshake { stage } = state(slot) {
            if stage == 131 {
                set_state(slot, State::Welcomed { stage: 0 });
                return send_welcome(slot);
            } else {
                set_state(slot, State::Handshake { stage: stage & !(1 << 7) });
            }
        }
    } else if cmd == "REQ" {
        write_reply!(slot, ":server CAP {} NAK :{}\r\n", n, tail.unwrap_or(""));
        reply(1 << slot);
    } else {
        write_reply!(slot, ":server 410 {} {} :Invalid CAP command\r\n", n, cmd);
        reply(1 << slot);
    }

    Ok(())
}

fn nick_command(slot: usize, nickname: Option<&str>) -> io::Result<()> {
    let nickname = match nickname {
        Some(n) => n,
        None => {
            return needs_more_params(slot, "NICK");
        }
    };

    let n = S::from_str(nickname);
    if nickname.starts_with('#') || nickname.starts_with('&') || nickname.starts_with(':') {
        if let State::Handshake { stage } = state(slot) {
            set_state(slot, State::Handshake { stage: stage & !1 });
            write_reply!(slot, ":server 432 {} {} :Erroneous nickname\r\n", addr(slot), n.as_str());
        } else {
            write_reply!(slot, ":server 432 {}!{}@{} {} :Erroneous nickname\r\n", nick(slot), username(slot), addr(slot), n.as_str());
        }
        reply(1 << slot);
        return Ok(());
    }

    let truncated = n.as_str();
    for s in bits(connected(0)) {
        if nick(s) == truncated {
            if let State::Handshake { stage } = state(slot) {
                write_reply!(slot, ":server 436 {} {} :Nickname collision KILL {}@{}\r\n", addr(slot), n.as_str(), username(s), addr(s));
                set_state(slot, State::Handshake { stage: stage & !1 });
            } else {
                write_reply!(slot, ":server 433 {} {} :Nickname is already in use\r\n", nick(slot), n.as_str());
            }

            reply(1 << slot);
            return Ok(());
        }
    }

    let previous = unsafe {
        let prev = NICKS[slot];
        NICKS[slot] = n;
        prev
    };

    if let State::Handshake { stage } = state(slot) {
        if stage == 2 {
            set_state(slot, State::Welcomed { stage: 0 });
            return send_welcome(slot);
        } else {
            set_state(slot, State::Handshake { stage: stage | 1 });
        }
    } else {
        write_outbound!(slot, ":{} NICK {}\r\n", previous.as_str(), nick(slot));
        outbound(connected(0));
    }
    Ok(())
}

fn user_command(slot: usize, uname: Option<&str>, zero: Option<&str>, star: Option<&str>, rname: Option<&str>) -> io::Result<()> {
    let stage = match state(slot) {
        State::Handshake { stage } => stage,
        _ => return already_registered(slot),
    };

    match (uname, zero, star, rname) {
        (Some(uname), Some(_), Some(_), Some(rname)) => unsafe {
            USERNAMES[slot] = S::from_str(uname);
            REALNAMES[slot] = S::from_str(rname);
        },
        _ => return needs_more_params(slot, "USER"),
    }

    if stage == 1 {
        set_state(slot, State::Welcomed { stage: 0 });
        return send_welcome(slot);
    } else {
        set_state(slot, State::Handshake { stage: stage | 2 });
    }

    Ok(())
}

fn send_welcome(slot: usize) -> io::Result<()> {
    let stage = match state(slot) {
        State::Welcomed { stage } => stage,
        _ => return Ok(()),
    };

    match stage {
        0 => write_reply!(slot, ":server 001 {} :Welcome to the network, {}!{}@{}\r\n", nick(slot), nick(slot), username(slot), addr(slot)),
        1 => write_reply!(slot, ":server 002 {} :Your host is server, running version 1.0\r\n", nick(slot)),
        2 => write_reply!(slot, ":server 003 {} :This server was created {}\r\n", nick(slot), unsafe { CREATION_TIME.assume_init() }),
        3 => write_reply!(slot, ":server 004 {} server 1 * *\r\n", nick(slot)),
        4 => write_reply!(slot, ":server 005 {} CHANNELLEN=32 NICKLEN=8 CHANLIMIT=#:64 CASEMAPPING=ascii CHANTYPES=#,& :are supported by this server\r\n", nick(slot)),
        5 => write_reply!(slot, ":server 422 {} :MOTD file is missing\r\n", nick(slot)),
        _ => (),
    }

    reply(1 << slot);

    if stage == 5 {
        set_state(slot, State::Ready);
    } else {
        set_state(slot, State::Welcomed { stage: stage + 1 });
        delayed(1 << slot);
    }

    Ok(())
}

fn ping_command(slot: usize, token: Option<&str>) -> io::Result<()> {
    if let Some(t) = token {
        write_reply!(slot, ":server PONG server {}\r\n", t);
        reply(1 << slot);
        Ok(())
    } else {
        return needs_more_params(slot, "PING");
    }
}

fn join_command(slot: usize, channels: Option<&str>) -> io::Result<()> {
    match state(slot) {
        State::Ready => (),
        _ => return Ok(()),
    }

    if channels.is_none() {
        return needs_more_params(slot, "JOIN");
    }

    let mut joining = 0u64;
    for name in channels.unwrap().split(',') {
        if name.is_empty() {
            return no_such_channel(slot, name);
        }

        if let Some(i) = find_chan(name) {
            joining |= 1 << i;
        } else if let Some(i) = create_channel(slot, name) {
            joining |= 1 << i;
        } else {
            return no_such_channel(slot, name);
        }
    }

    delay(slot, Delay::Joining { channels: joining });
    Ok(())
}

fn join(slot: usize, channels: u64) -> io::Result<()> {
    match bits(channels).next() {
        Some(ch) => {
            if 0 == (members(ch) & (1 << slot)) {
                unsafe { MEMBERS[ch] |= 1 << slot };
                write_outbound!(slot, ":{} JOIN {}\r\n", nick(slot), channame(ch));
                outbound(members(ch));
                let d = Delay::Joining { channels: channels & !(1 << ch) };
                set_state(slot, State::Delayed(d));
                delay(slot, d);
            } else {
                return names(slot, channels, 0, true);
            }
        }
        None => set_state(slot, State::Ready),
    }

    Ok(())
}

fn names(slot: usize, channels: u64, sent_members: u64, was_joining: bool) -> io::Result<()> {
    let channel = match bits(channels).next() {
        Some(c) => c,
        None => {
            set_state(slot, State::Ready);
            return Ok(());
        }
    };

    let remaining = members(channel) & !sent_members;
    if remaining == 0 {
        write_reply!(slot, ":server 366 {} {} :End of /NAMES list\r\n", nick(slot), channame(channel));
        reply(1 << slot);

        let others = channels & !(1 << channel);
        if let Some(c) = bits(others).next() {
            if 0 == (1 << slot) & members(c) && was_joining {
                delay(slot, Delay::Joining { channels: others });
            } else {
                delay(slot, Delay::Names { channels: others, sent_members: 0, was_joining });
            }
        } else {
            set_state(slot, State::Ready);
        }

        return Ok(());
    }

    let mut len = 18 + nick(slot).len() + channame(channel).len();
    let mut included = 0;
    for s in bits(members(channel)) {
        if len + nick(s).len() > 512 {
            break;
        }
        len += nick(s).len();
        included |= 1 << s;
    }

    unsafe {
        REPLY.clear();
        write!(&mut REPLY, ":server 353 {} = {} ", nick(slot), channame(channel))?;
        let mut leader = true;
        for s in bits(included) {
            write!(&mut REPLY, "{}{}", if leader { ':' } else { ' ' }, nick(s))?;
            leader = false;
        }
        write!(&mut REPLY, "\r\n")?;
    }

    reply(1 << slot);
    let d = Delay::Names {
        channels,
        sent_members: sent_members | included,
        was_joining,
    };
    set_state(slot, State::Delayed(d));
    delay(slot, d);
    Ok(())
}

fn privmsg_command(slot: usize, target: Option<&str>, msg: Option<&str>) -> io::Result<()> {
    match (target, msg) {
        (Some(t), Some(m)) => {
            if m.is_empty() {
                return Ok(());
            }

            let targets = if t.starts_with('#') || t.starts_with('&') {
                if let Some(i) = find_chan(t) {
                    members(i) & !(1u64 << slot)
                } else {
                    return no_such_channel(slot, t);
                }
            } else {
                if let Some(i) = find_nick(t) {
                    1u64 << i
                } else {
                    return no_such_nick(slot, t);
                }
            };

            write_outbound!(slot, ":{} PRIVMSG {} :{}\r\n", nick(slot), t, m);
            outbound(targets);
            Ok(())
        }
        _ => return needs_more_params(slot, "PRIVMSG"),
    }
}

fn mode_command(slot: usize, tgt: Option<&str>) -> io::Result<()> {
    if let Some(target) = tgt {
        if target.starts_with('#') || target.starts_with('&') {
            if let Some(_) = bits(unsafe { CHANNELS }).position(|c| channame(c) == target) {
                write_reply!(slot, ":server 324 {} {} :\r\n", nick(slot), target);
                reply(1 << slot);
            } else {
                return no_such_channel(slot, target);
            }
        } else {
            if target == nick(slot) {
                write_reply!(slot, ":server 221 {} :\r\n", nick(slot));
                reply(1 << slot);
            } else {
                write_reply!(slot, ":server 502 {} :Cant change modes for other users\r\n", nick(slot));
                reply(1 << slot);
            }
        }

        return Ok(());
    }

    return needs_more_params(slot, "MODE");
}

fn part_command(slot: usize, tgts: Option<&str>) -> io::Result<()> {
    match state(slot) {
        State::Ready => (),
        _ => return Ok(()),
    }

    if tgts.is_none() {
        return needs_more_params(slot, "PART");
    }

    let mut parting = 0u64;
    for name in tgts.unwrap().split(',') {
        if name.is_empty() {
            return no_such_channel(slot, name);
        }

        if let Some(i) = find_chan(name) {
            parting |= 1 << i;
        } else {
            return no_such_channel(slot, name);
        }
    }

    delay(slot, Delay::Parting { channels: parting });
    Ok(())
}

fn part(slot: usize, channels: u64) -> io::Result<()> {
    match bits(channels).next() {
        Some(ch) => {
            if 0 == members(ch) & (1 << slot) {
                return not_on_channel(slot, channame(ch));
            }

            write_outbound!(slot, ":{} PART {}\r\n", nick(slot), channame(ch));
            outbound(members(ch));
            unsafe { MEMBERS[ch] &= !(1 << slot) };
            let d = Delay::Parting { channels: channels & !(1 << ch) };
            set_state(slot, State::Delayed(d));
            delay(slot, d);

            if 0 == members(ch) {
                destroy_channel(ch);
            }
        }
        None => set_state(slot, State::Ready),
    }
    Ok(())
}

fn quit_command(slot: usize, reason: Option<&str>) -> io::Result<()> {
    write_outbound!(slot, ":{} QUIT :Reason: {}\r\n", nick(slot), reason.unwrap_or("No reason given"));
    write_reply!(slot, ":server ERROR :You have quit\r\n",);
    outbound(connected(0) & !(1 << slot));
    reply(1 << slot);
    close(1 << slot);
    set_state(slot, State::Disconnected);
    Ok(())
}

fn list_command(slot: usize, tgts: Option<&str>) -> io::Result<()> {
    match state(slot) {
        State::Ready => (),
        _ => return Ok(()),
    }

    let channels = match tgts {
        Some(targets) => {
            let mut mask = 0u64;
            for name in targets.split(',') {
                if let Some(i) = find_chan(name) {
                    mask |= 1 << i;
                } else {
                    return no_such_channel(slot, name);
                }
            }
            mask
        }
        None => unsafe { CHANNELS },
    };

    write_reply!(slot, ":server 321 {} Channel :Users Name\r\n", nick(slot));
    reply(1 << slot);
    delay(slot, Delay::Listing { channels });
    Ok(())
}

fn list(slot: usize, chans: u64) -> io::Result<()> {
    match bits(chans).next() {
        Some(ch) => {
            write_reply!(slot, ":server 322 {} {} {} :\r\n", nick(slot), channame(ch), members(ch).count_ones());
            reply(1 << slot);
            let d = Delay::Listing { channels: chans & !(1 << ch) };
            set_state(slot, State::Delayed(d));
            delay(slot, d);
        }
        None => {
            write_reply!(slot, ":server 323 {} :End of /LIST\r\n", nick(slot));
            reply(1 << slot);
            set_state(slot, State::Ready);
        }
    }

    Ok(())
}

fn who_command(slot: usize, mask: Option<&str>) -> io::Result<()> {
    match mask {
        Some(chan) if chan.starts_with('#') || chan.starts_with('&') => {
            if let Some(ix) = find_chan(chan) {
                delay(slot, Delay::WhoIs { users: members(ix), channel: ix as u8 });
            } else {
                return no_such_channel(slot, chan);
            }
        }
        Some(user) => {
            if let Some(ix) = find_nick(user) {
                delay(slot, Delay::WhoIs { users: 1 << ix, channel: 64u8 });
            } else {
                return no_such_nick(slot, user);
            }
        }
        None => {
            delay(slot, Delay::WhoIs { users: connected(0), channel: 64u8 });
        }
    }
    Ok(())
}

fn who(slot: usize, users: u64, channel: u8) -> io::Result<()> {
    let c = if channel == 64 { "*" } else { channame(channel as usize) };
    match bits(users).next() {
        Some(u) => {
            write_reply!(slot, ":server 352 {} {} {} {} server {} * :0 {}\r\n", nick(slot), c, username(u), addr(u), nick(u), realname(u));
            let d = Delay::WhoIs { users: users & !(1 << u), channel };
            delay(slot, d);
            set_state(slot, State::Delayed(d));
        }
        None => {
            write_reply!(slot, ":server 315 {} {} :End of WHO list\r\n", nick(slot), c);
            set_state(slot, State::Ready);
        }
    }
    reply(1 << slot);
    Ok(())
}

fn unknown_command(slot: usize, cmd: &str) -> io::Result<()> {
    write_reply!(slot, ":server 421 {} {} :Unknown command\r\n", nick(slot), cmd);
    reply(1 << slot);
    Ok(())
}

fn needs_more_params(slot: usize, cmd: &str) -> io::Result<()> {
    write_reply!(slot, ":server 461 {} {} :Not enough parameters\r\n", nick(slot), cmd);
    reply(1 << slot);
    Ok(())
}

fn already_registered(slot: usize) -> io::Result<()> {
    write_reply!(slot, ":server 462 {} :You may not reregister\r\n", nick(slot));
    reply(1 << slot);
    Ok(())
}

fn message_too_long<T>(slot: usize) -> io::Result<T> {
    write_reply!(slot, ":server 417 {} :Input line was too long\r\n", nick(slot));
    reply(1 << slot);
    Err(io::Error::new(io::ErrorKind::Other, "Input too long"))
}

fn no_such_channel(slot: usize, channel: &str) -> io::Result<()> {
    write_reply!(slot, ":server 403 {} {} :No such channel\r\n", nick(slot), channel);
    reply(1 << slot);
    Ok(())
}

fn no_such_nick(slot: usize, target: &str) -> io::Result<()> {
    write_reply!(slot, ":server 401 {} {} :No such nick\r\n", nick(slot), target);
    reply(1 << slot);
    Ok(())
}

fn not_on_channel(slot: usize, chan: &str) -> io::Result<()> {
    write_reply!(slot, ":server 402 {} {} :You're not on that channel\r\n", nick(slot), chan);
    reply(1 << slot);
    Ok(())
}

fn send(slot: usize) -> io::Result<()> {
    if !can_write(slot) {
        eprintln!("[{}] {} Delaying write", slot, now());
        delay(slot, Delay::Retry);
        return Ok(());
    }

    unsafe {
        let conn = CONNS[slot].assume_init_mut();
        let buffer = if should_use_reply(slot) { &REPLY[..] } else { &OUTPUT[..] };

        println!("[{}] {} > {:?}", slot, now(), str::from_utf8_unchecked(buffer));

        if let Err(e) = conn.write(buffer) {
            if e.kind() == io::ErrorKind::WouldBlock {
                delay(slot, Delay::Retry);
                WRITE &= !(1 << slot);
            } else {
                close(1 << slot);
                return Err(e);
            }
        }

        RESPOND &= !(1 << slot);
        USE_REPLY &= !(1 << slot);
    }

    Ok(())
}

fn message(slot: usize) -> io::Result<(&'static str, usize)> {
    let bytes = unsafe {
        let conn = CONNS[slot].assume_init_mut();
        let existing = INPUT.len();
        INPUT.set_len(INPUT.capacity());
        let len = match conn.read(&mut INPUT[existing..]) {
            Ok(0) => {
                close(1 << slot);
                INPUT.set_len(0);
                return Err(io::Error::new(io::ErrorKind::Other, "Connection closed"));
            }
            Ok(n) => existing + n,
            Err(e) if e.kind() == io::ErrorKind::WouldBlock => {
                READ &= !(1 << slot);
                existing
            }
            Err(e) => {
                INPUT.set_len(0);
                return Err(e);
            }
        };

        INPUT.set_len(len);
        &INPUT[..]
    };

    let end = bytes.windows(2).position(|x| x[0] == b'\r' && x[1] == b'\n').map(|x| x + 2).or(bytes.iter().position(|x| *x == b'\n').map(|x| x + 1));

    match end {
        Some(n) => match str::from_utf8(unsafe { &INPUT[..n] }) {
            Ok(s) => {
                if n >= unsafe { INPUT.len() } {
                    unsafe {
                        READ &= !(1 << slot);
                    }
                }
                Ok((s, n))
            }
            Err(_) => {
                unsafe { INPUT.clear() }
                Err(io::Error::new(io::ErrorKind::Other, "Invalid utf-8"))
            }
        },
        None => return message_too_long(slot),
    }
}

fn flush() {
    for slot in bits(outbound(0)) {
        if let Err(e) = send(slot) {
            eprintln!("[{}] {} Output: {}", slot, now(), e);
        }
    }

    for slot in bits(close(0)) {
        if let Err(e) = disconnect(slot) {
            eprintln!("[{}] {} Closing: {}", slot, now(), e);
        }
    }
}

fn connected(mask: u64) -> u64 {
    unsafe {
        CLIENTS |= mask;
        CLIENTS
    }
}
fn readable(mask: u64) -> u64 {
    unsafe {
        READ |= mask;
        READ
    }
}
fn writable(mask: u64) -> u64 {
    unsafe {
        WRITE |= mask;
        WRITE
    }
}
fn close(mask: u64) -> u64 {
    unsafe {
        CLOSE |= mask;
        CLOSE
    }
}
fn outbound(mask: u64) -> u64 {
    unsafe {
        RESPOND |= mask;
        RESPOND
    }
}
fn delayed(mask: u64) -> u64 {
    unsafe {
        DELAYED |= mask;
        DELAYED
    }
}
fn reply(mask: u64) -> u64 {
    outbound(mask);
    unsafe {
        USE_REPLY |= mask;
        USE_REPLY
    }
}
fn should_use_reply(slot: usize) -> bool { unsafe { 0 != USE_REPLY & (1 << slot) } }
fn client(slot: usize) -> bool { unsafe { 0 != CLIENTS & (1 << slot) } }
fn state(slot: usize) -> State { unsafe { STATES[slot] } }
fn set_state(slot: usize, state: State) {
    eprintln!("[{}] {} State: {:?}", slot, now(), state);
    unsafe { STATES[slot] = state }
}

fn nick(slot: usize) -> &'static str { unsafe { NICKS[slot].as_str() } }
fn now() -> chrono::DateTime<chrono::Utc> { chrono::Utc::now() }
fn should_read(slot: usize) -> bool { 0 != readable(0) & (1 << slot) }
fn can_write(slot: usize) -> bool { 0 != writable(0) & (1 << slot) }
fn delay(slot: usize, delay: Delay) {
    if 0 == (delayed(0) & (1 << slot)) {
        delayed(1 << slot);
        match state(slot) {
            State::Delayed(_) => (),
            _ => set_state(slot, State::Delayed(delay)),
        }
    }
}

fn split_msg(mut msg: &str) -> (Option<&str>, &str, Option<&str>) {
    let prefix = if msg.starts_with(':') {
        if let Some((start, end)) = msg.split_once(' ') {
            msg = end;
            Some(start)
        } else {
            None
        }
    } else {
        None
    };

    let suffix = if msg.contains(':') {
        if let Some((middle, end)) = msg.split_once(':') {
            msg = middle;
            Some(end)
        } else {
            unreachable!()
        }
    } else {
        None
    };

    (prefix, msg, suffix)
}

fn abandon<T>(slot: usize, e: io::Error) -> io::Result<T> {
    close(1 << slot);
    unsafe {
        RESPOND = 0;
        USE_REPLY = 0;
    }
    Err(e)
}

fn addr(slot: usize) -> &'static net::IpAddr { unsafe { ADDRS[slot].assume_init_ref() } }
fn username(slot: usize) -> &'static str { unsafe { USERNAMES[slot].as_str() } }
fn realname(slot: usize) -> &'static str { unsafe { REALNAMES[slot].as_str() } }
fn channame(slot: usize) -> &'static str { unsafe { CHANNEL_NAMES[slot].as_str() } }
fn members(slot: usize) -> u64 { unsafe { MEMBERS[slot] } }

fn dump() {
    println!("CLIENT TABLE");
    println!("{:4} {:8} {:32} {:32} {:15} {}", "SLOT", "NICK", "REALNAME", "USERNAME", "ADDR", "STATE");
    for slot in bits(connected(0)) {
        println!("{:4} {:8} {:32} {:32} {:15} {:?}", slot, nick(slot), realname(slot), username(slot), addr(slot), state(slot));
    }

    println!();
    println!("CHANNEL TABLE");
    println!("{:4} {:32} {:64}", "SLOT", "NAME", "MEMBERS");
    for slot in bits(unsafe { CHANNELS }) {
        println!("{:4} {:32} {:064b}", slot, unsafe { CHANNEL_NAMES[slot].as_str() }, unsafe { MEMBERS[slot] });
    }
}

fn create_channel(slot: usize, name: &str) -> Option<usize> {
    unsafe {
        let ch = CHANNELS.trailing_ones() as usize;
        if ch > 63 {
            eprintln!("[{}] {} Cannot create channel {}: all slots full", slot, now(), name);
            return None;
        }

        CHANNEL_NAMES[ch] = S::from_str(name);
        CHANNELS |= 1 << ch;
        Some(ch)
    }
}
fn destroy_channel(channel: usize) { unsafe { CHANNELS &= !(1 << channel) } }
fn find_chan(c: &str) -> Option<usize> { bits(unsafe { CHANNELS }).position(|s| c == channame(s)) }
fn find_nick(n: &str) -> Option<usize> { bits(connected(0)).position(|s| n == nick(s)) }
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct S<const N: usize>([u8; N]);
impl<const N: usize> S<N> {
    pub const fn new() -> S<N> { S([0; N]) }
    pub fn from_str(s: &str) -> S<N> {
        let len = s.len().min(N);
        let mut buf = [0u8; N];
        buf[..len].copy_from_slice(&s.as_bytes()[..len]);
        S(buf)
    }
    pub fn as_str(&self) -> &str {
        let len: usize = self.0.into_iter().position(|x| x == 0).unwrap_or(N) as usize;
        unsafe { std::str::from_utf8_unchecked(&self.0[..len]) }
    }
}
impl<const N: usize> Display for S<N> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{}", self.as_str()) }
}
pub struct IterBits(u64, usize);
impl Iterator for IterBits {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        if self.0 == 0 {
            return None;
        }
        let shift = self.0.trailing_zeros();
        let position = self.1 + shift as usize;
        self.0 >>= 1 + shift;
        self.1 = 1 + position;
        Some(position)
    }
}
pub fn bits(mask: u64) -> IterBits { IterBits(mask, 0) }
