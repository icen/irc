# irc

This is a tiny, masochistically-minimal, statically-allocated(*) irc server,
written in rust :crab::crab::crab:

It supports up to 64 users, 64 channels, no topics, no capabilities, no TLS, 
no modes (not even ops).

It supports the following commands:

* CAP, only to report that there are none
* PASS, which is ignored
* NICK, accepting nicks up to 8 characters long
* USER, as per RFC 2812
* JOIN, as per RFC 2812, except the special "0" parameter, and with no support 
  for keys (they are ignored)
* PRIVMSG, because how else would anything work
* PART, as per RFC 2812, but ignores any reason given
* QUIT, as per RFC 2812
* PING, as per RFC 2812
* MODE, to report that there are no modes set for anything
* LIST, as per RFC 2812

Anything not otherwise mentioned is unsupported and unimplemented. If something
works that's not listed, either I forgot to update this document or you are 
very lucky!


(*): The `mio::Events` has a dynamic allocation that I'm not otherwise able to 
avoid.

## Running

The server runs on port 36667 by default. This is not configurable, but should
be easy to change if desired. 

No files on disk are read or written to.

Channel communications are written to stdout, with the following uniform
format:

`[<connection id>] <timestamp> > outbound message`, or
`[<connection id>] <timestamp> < inbound message`

Diagnostic information is written to stderr.

